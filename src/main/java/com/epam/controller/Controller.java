package com.epam.controller;

import java.util.List;

/**
 * Interface controller.
 * Manages options for simulators.
 */
public interface Controller {
    /**
     * Method which return list.
     *
     * @return getOccupationList.
     */
    List<String> getOccupationList();

    /**
     * Takes from model method weightFeet(int weight).
     * @param weight - this is user weight.
     */
    void weightFeet(int weight);

    /**
     * Takes from model method weightLostSimulator().
     */
    void weightLostSimulator();

    /**
     * Takes from model method pressSimulator().
     */
    void pressSimulator();

    /**
     * Takes from model method bodySimulator().
     */
    void bodySimulator();

    /**
     * Takes from model method help().
     */
    void help();
}
