package com.epam.controller;

import com.epam.model.BussinesLogic;
import com.epam.model.Model;

import java.util.List;

/**
 * This class transfers data from the model to View.
 */
public class Controllermpl implements Controller {
    /**
     * variable with connection to BussinesLogic.
     */
    private Model model;

    /**
     * Creates a new Controller with model.
     */
    public Controllermpl() {
        model = new BussinesLogic();
    }

    @Override
    public final List<String> getOccupationList() {
        return model.getOccupationList();
    }

    @Override
    public final void weightFeet(int weight) {
        model.weightFeet(weight);
    }

    @Override
    public final void weightLostSimulator() {
        model.weightLostSimulator();
    }

    @Override
    public final void pressSimulator() {
        model.pressSimulator();
    }

    @Override
    public final void bodySimulator() {
        model.bodySimulatr();
    }

    @Override
    public final void help() {
        model.help();
    }
}
