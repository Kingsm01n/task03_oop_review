package com.epam.model;

import java.util.LinkedList;
import java.util.List;

public class Domain {
    /**
     * List with dates for special simulators.
     */
    private List<String> occupationList;
    /**
     * weight user.
     */
    private int weight;
    /**
     * calculates weight for user.
     */
    private int platformSim = 15;
    /**
     * calculates weight for user.
     */
    private int legSim = 30;

    /**
     * Creates a new Domain with selected occupation list.
     */
    public Domain() {
        occupationList = new LinkedList<String>();
    }

    /**
     * writes data weight lost simulator to the list.
     */
    public final void weightLostTrainer() {
        occupationList.clear();
        occupationList.add(" Treadmill - 20 minutes");
        occupationList.add("\n  Exercise bike - 30 minutes (speed = 15km/h)");
        occupationList.add("\n  Press - 5 approaches to 15 times ");
        occupationList.add("\n  Elliptical trainer - 10 minutes  ");
        occupationList.add("\n  Stepper - 10 minutes  ");

    }

    /**
     * writes data feet simulator to the list.
     * Calculated working weight.
     *
     * @param weight this is a weight user.
     *               User must input his weight.
     */
    public final void feetSimulator(int weight) {
        this.weight = weight;


        occupationList.clear();
        occupationList.add(" \nHackenschmidt simulator -"
                + " 4 apporoaches 10 times (weight = " + weight + ")");
        occupationList.add("\n  Platform simulator -"
                + " 4 apporoaches 10 times (weight = "
                + (weight - platformSim) + ")");
        occupationList.add("\n  Leg extension in knees while sitting -"
                + " 4 apporoaches 10 times (weight = "
                + (weight - legSim) + ")");
        occupationList.add("\n  Bending of legs in knees lying down -"
                + " 4 apporoaches 10 times (weight = "
                + (weight - legSim) + ")");
        occupationList.add("\n  The simulator for the calves -"
                + " 4 apporoaches 15 times (weight = "
                + (weight - legSim) + ")");
    }

    /**
     * Assists the program.
     */
    public final void help() {
        System.out.println("if you want lose you weight input - 1,"
                + " else input 2, 3 or 4.");
    }

    /**
     * writes data body simulator to the list.
     */
    public final void bodySimulator() {
        occupationList.clear();
        occupationList.add(" Butterfly simulator - 5 approaches 15 times ");
        occupationList.add("\n  Hammer simulator - approaches 15 times");
        occupationList.add("\n  Crossover - 5 approaches 15 times ");
    }

    /**
     * writes data press simulator to the list.
     */
    public final void pressSimulator() {
        occupationList.clear();
        occupationList.add(" Inclined lava - 5 approaches 15 times ");
        occupationList.add("\n  Gymnastic roller - approaches 15 times");
        occupationList.add("\n  \"Roman chair\" - 5 approaches 15 times ");
    }

    /**
     * print data for special simulators.
     * @return occupationList
     */
    public final List<String> getOccupationList() {
        return occupationList;
    }

    /**
     * returns weight of user.
     * @return weight
     */
    public final int getWeight() {
        return weight;
    }

}
