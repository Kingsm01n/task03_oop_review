package com.epam.model;

import java.util.List;

/**
 * Interface Model.
 * Have some methods which takes special data.
 */
public interface Model {
    /**
     * Method must return list.
     *
     * @return getOccupationList
     */
    List<String> getOccupationList();

    /**
     * Method which shows workout for feet.
     * Takes 1 argument - weight
     * user must input his weight.
     *
     * @param weight this is user weight.
     */
    void weightFeet(int weight);

    /**
     * Method which shows help.
     */
    void help();

    /**
     * Method which shows list for weight lost.
     */
    void weightLostSimulator();

    /**
     * Method which shows list for press.
     */
    void pressSimulator();

    /**
     * Method which shows list for body.
     */
    void bodySimulatr();


}
