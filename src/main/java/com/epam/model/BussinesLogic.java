package com.epam.model;

import java.util.List;

/**
 * The class constaint bussines logic of program.
 */
public class BussinesLogic implements Model {
    /**
     * domain constaint data.
     */
    private Domain domain;
    /**
     * // min weight user for training.
     */
    private int minWeight = 40;

    /**
     * Create a new BussinesLogic with domain.
     */
    public BussinesLogic() {
        domain = new Domain();
    }

    @Override
    public final List<String> getOccupationList() {
        return domain.getOccupationList();
    }

    @Override
    public final void weightFeet(int weight) {
        if (weight > minWeight) {
            domain.feetSimulator(weight);
        }
    }

    @Override
    public final void help() {
        domain.help();
    }

    @Override
    public final void weightLostSimulator() {
        domain.weightLostTrainer();
    }

    @Override
    public final void pressSimulator() {
        domain.pressSimulator();
    }

    @Override
    public final void bodySimulatr() {
        domain.bodySimulator();
    }
}
