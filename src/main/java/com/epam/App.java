package com.epam;

import com.epam.view.MyView;
/**
 * <h1>oop homework</h1>
 *
 * @author Maksym Kuziv
 * @version 1.0
 * @since 2019-11-10
 */

/**
 * My second Maven project written in IntelliJ IDEA.<br>
 * To run main method start simulatiors helper.
 */
public class App {
    /**
     * This is the entry point of my homework
     *
     * @param args
     */
    public static void main(String[] args) {
        MyView myView = new MyView();
        myView.show();
    }
}
