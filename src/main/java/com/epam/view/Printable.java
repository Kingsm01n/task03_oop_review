package com.epam.view;

/**
 * Interface which have a method print.
 */
public interface Printable {
    /**
     * The method print some list.
     */
    void print();
}
