package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.Controllermpl;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;


/**
 * Class MyView shows for user menu, provides choice.
 */
public class MyView {
    /**
     * Controller variable for methods.
     */
    private Controller controller;
    /**
     * Map contains list menu.
     */
    private Map<String, String> menu;
    /**
     * Map contains methods menu.
     */
    private Map<String, Printable> methodsMenu;
    /**
     * Scanner for choice menu point.
     */
    private static Scanner input = new Scanner(System.in);
    /**
     * Min weight user.
     */
    private int minWeight = 40;

    /**
     *Creates new MyView with new Controller, menu and methodsMenu.
     */
    public MyView() {
        controller = new Controllermpl();
        menu = new LinkedHashMap<String, String>();
        menu.put("1", "1 - Lost weight simulators");
        menu.put("2", "2 - Feet simulators");
        menu.put("3", "3 - Press simulators");
        menu.put("4", "4 - Body simulators");
        menu.put("5", "5 - HELP");
        menu.put("Q", "Q - exit");

        methodsMenu = new LinkedHashMap<String, Printable>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    /**
     * The first poin of menu.
     * Shows list widthLostSimulator.
     */
    private void pressButton1() {
        controller.weightLostSimulator();
        System.out.println(controller.getOccupationList());
    }
    /**
     * The first poin of menu.
     * Shows list feetSimulator.
     * Calculated weight workout.
     */
    private void pressButton2() {
        System.out.print("Input your weight (>40) :");
        int weight = Integer.parseInt(input.nextLine());
        if (weight <= minWeight) {
            System.out.println("Weight must be more than 40");
            controller.weightFeet(weight);
        } else {
            controller.weightFeet(weight);
            System.out.println(controller.getOccupationList());
        }
    }
    /**
     * The first poin of menu.
     * Shows list pressSimulator.
     */
    private void pressButton3() {
        controller.pressSimulator();
        System.out.println(controller.getOccupationList());
    }
    /**
     * The first poin of menu.
     * Shows list bodySimulator.
     */
    private void pressButton4() {
        controller.bodySimulator();
        System.out.println(controller.getOccupationList());
    }
    /**
     * The first poin of menu.
     * Shows help.
     */
    private void pressButton5() {
        controller.help();
    }
    /**
     * The first poin of menu.
     * Shows menu.
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     * Method which provides the opportunity for choice menu point.
     * User must choice point.
     * If user input "Q" program is excluded.
     * keyMap - user choice.
     */
    public final void show() {
        String keyMap;
        do {
            outputMenu();
            System.out.println("Select menu point.");
            keyMap = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMap).print();
            } catch (Exception e) {
            }
        } while (!keyMap.equals("Q"));

    }

}
